import pandas as pd
import numpy as np
from IPython.display import display
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from skfeature.function.similarity_based import fisher_score
import itertools
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt
from sklearn import preprocessing
from scipy.spatial.distance import cdist
from scipy import stats
from sklearn.neighbors import KNeighborsClassifier
import datetime
import seaborn as sb


KSEL = False
FISHER = True
CORRMATRIX = False
KNN = False

#To get better visual of the confusion matrix:
def plot_confusion_matrix(cm, classes,
             normalize=False,
             title='Confusion matrix',
             cmap=plt.cm.Blues):
    #Add Normalization Option
    '''prints pretty confusion metric with normalization option '''
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)
    
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt), horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")
    
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


df = pd.read_csv("smoke_detection_iot.csv")
df = df.fillna(0)
df.info()
#display(df)
df1 = df[df['Fire Alarm'] == 1]
df2 = df[df['Fire Alarm'] == 0]

df1.info()
#display(df1)

df2.info()
#display(df2)
#there are 26.884 more entry for fire alarm = 1 than in fire alarm = 0 --> need to balance the dataset 

df.sort_values('Fire Alarm', inplace=True, ascending=False)

df = df.iloc[26884:]

#shuffle objects in the dataset 
df = df.sample(frac=1, random_state=42)
df.info()

#now we have 17873 entries for each class 

#need to manage the UTC --> seems reasonable to only keep the month(?)
df['UTC'] = df['UTC'].apply(lambda x: datetime.datetime.fromtimestamp(x).month)


df.to_csv("Smoke_detection_set.csv")
df
#since the month is equal for all sampoles, we are not going to consider it 


MinMaxScaler = preprocessing.MinMaxScaler()
y_data = pd.read_csv('Smoke_detection_set.csv', usecols= ['Fire Alarm'])
x_data = pd.read_csv('Smoke_detection_set.csv', usecols= ['Temperature[C]','Humidity[%]','TVOC[ppb]',
                                                    'eCO2[ppm]','Raw H2','Raw Ethanol','Pressure[hPa]',
                                                    'PM1.0','PM2.5','NC0.5','NC1.0',
                                                    'NC2.5'])


print("Class sample : ", y_data.shape)
print("Features samples : ", x_data.shape)

#take the 80% of data for train and 20% for test
x_train = x_data[:28596]#35746
y_train = y_data[:28596]

x_test = x_data[28596:]
y_test = y_data[28596:]

x_train = x_train.values
x_test = x_test.values

print("Classes train : ", y_train.shape)
print("Features train : ", x_train.shape)

print("Classes test :", y_test.shape)
print("Features train : ", x_test.shape)


##########################################SELECT K BEST FEATURES-CHI SQUARE METHOD#####################################################
if (KSEL):
    x_train = MinMaxScaler.fit_transform(x_train)
    x_test = MinMaxScaler.fit_transform(x_test)
    x_train_mask = SelectKBest(chi2, k=3)
    x_train_mask.fit_transform(x_train,y_train)
    print(x_train_mask.get_support())

#####################################FISHER FEATURE RANKING##########################
if (FISHER):
    features = ['Temperature[C]','Humidity[%]','TVOC[ppb]','eCO2[ppm]','Raw H2','Raw Ethanol','Pressure[hPa]','PM1.0','PM2.5','NC0.5','NC1.0','NC2.5']
    fisher_data = pd.read_csv('Smoke_detection_set.csv')
    fisher_data = fisher_data.sample(frac = 0.3, random_state = 33)
    display(fisher_data)
    y_fisher = fisher_data['Fire Alarm']
    x_fisher = fisher_data[features]

    y_fisher = np.asarray(y_fisher)
    x_fisher = np.asarray(x_fisher)
    y_fisher = np.reshape(y_fisher,-1)


    score = fisher_score.fisher_score(x_fisher,y_fisher)

    a = zip(features,score)
    sorted_features = sorted(a, key = lambda t:t[1])

    sorted_features = list(zip(*sorted_features))[0]
    print("sorted_feat: ", sorted_features)

    print("Score for:\nTemp: ", score[0], "\nHum: ", score[1], "\nTVOC: ", score[2], 
                    "\neCO2:", score[3], "\nH2:", score[4], "\nEthanol: ", score[5], "\nPressure: ", score[6],
                     "\nPM1.0: ", score[7], "\nPM2.5: ", score[8], "\nNC0.5", score[9], "\nNC1.0", score[10], "\nNC2.5", score[11] )

    #Print correlation matrix with feature sorted by fisher score

    x_data_fish = x_data[list(sorted_features)]
    display(x_data_fish)
    corr = x_data_fish.corr()
    plt.figure(figsize = (10,6))
    sb.heatmap(corr, annot=True) 
    plt.tight_layout() 
    plt.show()


    """x_data_numpy = np.asarray(x_train)
    y_data_numpy = np.asarray(y_train)
    y_data_numpy = np.reshape(y_data_numpy,-1)


    print(y_data_numpy)
    print(x_data_numpy)

    batchx_1 = x_data_numpy[:9532]
    batchx_2 = x_data_numpy[9532: 19064]
    batchx_3 = x_data_numpy[19064 : 28596]

    batchy_1 = y_data_numpy[:9532]
    batchy_2 = y_data_numpy[9532: 19064]
    batchy_3 = y_data_numpy[19064 : 28596]

    idx1 = fisher_score.fisher_score(batchx_1,batchy_1)
    
    idx2= fisher_score.fisher_score(batchx_2,batchy_2)

    idx3 = fisher_score.fisher_score(batchx_3,batchy_3)"""
##########################################FEATURES CORRELATION###############################################################
if (CORRMATRIX):
    corr = x_data.corr()
    plt.figure(figsize = (10,6))
    sb.heatmap(corr, annot=True)
    plt.show()


##########################################################KNN##############################
if (KNN):
    from sklearn.metrics import accuracy_score
    ks = [7,9,11,13,15]
    funs = ["euclidean", "manhattan","chebyshev","minkowski"]#,"seuclidean","mahalanobis"]

    classes = ['0','1']

    train_accuracy = np.empty(len(ks))
    test_accuracy = np.empty(len(ks))
    train_scores, test_scores = list(), list()

    #iterate over different functions
    for fun in funs:
        i=0
        for k in ks:
            if fun=="minkowski":
                knn_clf=KNeighborsClassifier(n_neighbors=k, weights='distance', metric=fun, p=8)
            elif fun == "manhattan":
                knn_clf=KNeighborsClassifier(n_neighbors=k, weights='distance', metric=fun, p=1)
            else:
                knn_clf=KNeighborsClassifier(n_neighbors=k, weights='distance', metric=fun)

            print("#################", fun ,"[K=%i]  #################" % k)
            knn_clf.fit(x_train,y_train)
            predict=knn_clf.predict(x_test)

     #print classification report
            classification_metrics = metrics.classification_report(y_test, np.round(predict), target_names=classes)
            cm_dict = metrics.classification_report(y_test, np.round(predict), target_names=classes, output_dict=True)
            print(classification_metrics)
            print(fun[0].capitalize()+fun[1:]+"\\\\")

            confusion_matrix= metrics.confusion_matrix(y_test, predict)

            # plot confusion matrix
            plot_confusion_matrix(confusion_matrix, classes)
            plt.show()
            plot_confusion_matrix(confusion_matrix, classes, normalize=True)
            plt.show()

            #storing accuracy for each k 
            test_accuracy[i] = knn_clf.score(x_test, y_test)
            i = i+1

        # graph to show accuracy 
        plt.plot(ks, test_accuracy, label = 'Testing dataset Accuracy')

        plt.legend()
        plt.xlabel('n_neighbors')
        plt.ylabel('Accuracy')
        plt.show()        